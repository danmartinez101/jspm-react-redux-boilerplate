import React, { Component } from 'react';
import HashHistory from 'react-router/lib/HashHistory';
import Router from './application/Router';

React.render(
  <Router history={new HashHistory()}/>,
  document.getElementById('root')
);
