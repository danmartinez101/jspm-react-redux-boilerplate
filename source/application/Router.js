import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { Router, Route } from 'react-router';

import createStore from '../createStore';
const store = createStore();

import Application from './index';

export default
class AppRouter extends Component {
  render() {
    return (
      <Provider store={store}>
        {()=>
          <Router history={this.props.history}>
            <Route path="/" component={Application}/>
          </Router>
        }
      </Provider>
    )
  }
}
