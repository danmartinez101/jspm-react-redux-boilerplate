import React, { Component } from 'react';
import { connect } from 'react-redux'
import hotify from 'react-hotify';

@hotify('Application')
@connect(state=>state)
export default
class Application extends Component {
  render() {
    return (
      <div>
        {
          this.props.children || <h1>jspm-react-redux-boilerplate</h1>
        }
      </div>
    )
  }
}
