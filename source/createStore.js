import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';

function reducer(state = {}, action) {
  return state;
}

const createStoreWithMiddleware = applyMiddleware(
  thunkMiddleware
)(createStore);

export default
function configureStore(initialState) {
  return createStoreWithMiddleware(reducer, initialState);
}
